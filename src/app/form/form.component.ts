import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { phoneValidator } from '../validate-phone.directive';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  profileForm!: FormGroup;

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.profileForm = new FormGroup({
      'userName': new FormControl(null, Validators.required),
      'surname': new FormControl(null, Validators.required),
      'middleName': new FormControl(null, Validators.required),
      'phone': new FormControl(null, [Validators.required, phoneValidator]),
      'job': new FormControl(null, Validators.required),
      'tshirt': new FormControl(null, Validators.required),
      'size': new FormControl(null, Validators.required),
      'comment': new FormControl(null, [Validators.required, Validators.maxLength(300)]),
      'skills': new FormArray([])
    });
  }

  onSubmit() {
    const body = {
      userName: this.profileForm.value.userName,
      surname: this.profileForm.value.surname,
      middleName: this.profileForm.value.middleName,
      phone: this.profileForm.value.phone,
      job: this.profileForm.value.job,
      tshirt: this.profileForm.value.tshirt,
      size: this.profileForm.value.size,
      comment: this.profileForm.value.comment,
      skills: this.profileForm.value.skills,
    };


    return this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/users.json', body).pipe(
      tap(() => {
        void this.router.navigate(['/thank-you'], {relativeTo: this.route})
      })
    ).subscribe();
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.profileForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  addSkill() {
    const skills = <FormArray>this.profileForm.get('skills');
    const skillGroup = new FormGroup({
      skill: new FormControl(null, Validators.required),
      level: new FormControl(null, Validators.required),
    });
    skills.push(skillGroup);
  }

  getSkillsControls() {
    return (<FormArray>this.profileForm.get('skills')).controls;
  }

  getFormValid() {
    return this.profileForm.valid;
  }
}
